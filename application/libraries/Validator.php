<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Validator
{

    function __construct()
    {
//         parent::__construct();
        $this->CI =& get_instance();
        $this->CI->load->config('validate_message');
        $this->CI->load->library('email');
    }

    /*
     * check for valid email-id
     * */
    public function isValidEmail($email)
    {
        $reg = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

        if (preg_match($reg, $email)) {
            return true;
        }
        return false;

    }

    /*
     * check  for valid mobile number..
     *
     * */

    public function validate_mobile_number($number)
    {
//        echo preg_match("/^[0-9]{10}*$/",$number)."<br/>";
//		echo strlen($number);

        if (!preg_match('/^0\d{10}$/', $number) && strlen($number) != 10) {
            return false;
        }
        return true;
    }

    /*
     * in this function params will be validated with post or get params
     * */

    public function valid_params($params, $requiredfields)
    {
        $error = "";
        $keys = array_keys($params);
        if (count($params) == count($requiredfields)) {
            for ($i = 0; $i < count($requiredfields); $i++) {
                $feild = $requiredfields[$i];
                if (in_array($feild, $keys)) {
                    if ($params[$feild] == "") {
                        $error = $error . $feild . ",";
                    }
                } else {
                    $error = $error . $feild . ",";
                }
            }
            $error = trim($error);
        } else {
            for ($i = 0; $i < count($requiredfields); $i++) {
                $feild = $requiredfields[$i];
                if (in_array($feild, $keys)) {
                    if ($params[$feild] == "") {
                        $error = $error . $feild . ",";
                    }
                } else {
                    $error = $error . $feild . ",";
                }
            }
            $error = trim($error);
        }
        if ($error != "") {
            $error = rtrim($error, ",");
            $response[$this->CI->config->item('status')] = false;
            $response[$this->CI->config->item('message')] = 'The following fields are required: ' . $error;
            return $response;
        }
        $response[$this->CI->config->item('status')] = true;
        $response[$this->CI->config->item('message')] = '';

        return $response;
    }

    function group_by($key, $data) {
        $result = array();
    
        foreach($data as $val) {
            if(array_key_exists($key, $val)){
                $result[$val->$key][] = $val;
            }else{
                $result[""][] = $val;
            }
        }
    
        return $result;
    }
    /**
     * sort date 
     * 
     */
    function date_sort($arr) {
        usort($arr, function($a,$b){
            return strtotime($a) - strtotime($b);
         }); 
        return $arr;
    }

    function sort_desc($a,$b) {
        echo 'hello';
        return strtotime($b) - strtotime($a);
    }
    
    public function do_upload($config, $name)
    {
        $response = array();
        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);
        if ($this->CI->upload->do_upload($name)) {
            $response[$this->CI->config->item('status')] = true;
            $response[$this->CI->config->item('data')] = $this->CI->upload->data();
            return $response;
        }
        $response[$this->CI->config->item('status')] = false;
        $response[$this->CI->config->item('message')] = $this->CI->upload->display_errors();
        return $response;
    }

    public function do_upload_multiple($path, $files)
    {
        $response = array();
        $title = time();

        $config = array(
            'upload_path'   => $path,
            'allowed_types' => '*',
            'overwrite'     => 1,                       
        );

        $this->CI->load->library('upload', $config);



        $files = $_FILES;
        $cpt = count($_FILES ['multipleUpload'] ['name']);

        $imagesNames = array();
        for ($i = 0; $i < $cpt; $i ++) {

            $name = time().rand(100,10000).$files ['multipleUpload']['name'] [$i];
            $_FILES ['multipleUpload'] ['name'] = $name;
            $_FILES ['multipleUpload'] ['type'] = $files ['multipleUpload'] ['type'] [$i];
            $_FILES ['multipleUpload'] ['tmp_name'] = $files ['multipleUpload'] ['tmp_name'] [$i];
            $_FILES ['multipleUpload'] ['error'] = $files ['multipleUpload'] ['error'] [$i];
            $_FILES ['multipleUpload'] ['size'] = $files ['multipleUpload'] ['size'] [$i];

            if(!($this->CI->upload->do_upload('multipleUpload')) || $files ['multipleUpload'] ['error'] [$i] !=0)
            {
                $response[$this->CI->config->item('status')] = false;
                $response[$this->CI->config->item('message')] = $this->CI->upload->display_errors();
                return $response;
            }
            else
            {
                $imagesNames[] = $name;
            }
        }
        $response[$this->CI->config->item('status')] = true;
        $response[$this->CI->config->item('message')] = "Images upload successfully";
        $response['data'] = $imagesNames;
        return $response;
    }

    function random_num($size)
    {
        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;

        $key = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }

     /* fcm send notification to single user*/    
    //  public function fcmMethod($data,$checkType=null,$userType='customer'){
    //     if($userType == "shopper"){
    //         define( 'API_ACCESS_KEY', $this->CI->config->item('FIREBASE_API_KEY_SHOPPER'));
    //     }
    //     else if($userType == "customer"){
    //         define( 'API_ACCESS_KEY', $this->CI->config->item('FIREBASE_API_KEY_CUSTOMER'));
    //     }
   
    //     $time = date('Y-m-d H:i:s');
    //     $time = new DateTime($time);
    //     $minutes_to_add = 1;
    //     $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
    //     $stamp = $time->format('M d,Y h:i A');
    //     if($checkType == ""){
    //     $checkType = "OPEN_ACTIVITY_1_U";
    //     } 
        
    //     $fcmMsg = array(
    //     'body' => $data['fcm_body'],
    //     'title' => $data['fcm_title'],
    //     'sound' => "default",
    //     'color' => "#203E78" ,
    //     "estimate"=> 100/*, 
    //     'click_action'=>$checkType */
    //     );
        
    //     $fcmFields = array(
    //     'to' => $data['to_user'],
    //     'priority' => 'high',
    //     'notification' => $fcmMsg
    //     );
       
    //     $headers = array(
    //         'Authorization: key=' . API_ACCESS_KEY,
    //         'Content-Type: application/json'
    //         );
        
       
        
    //     $ch = curl_init();
    //     curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    //     curl_setopt( $ch,CURLOPT_POST, true );
    //     curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    //     curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    //     curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    //     curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    //     $result = curl_exec($ch );
    //     curl_close( $ch );
        
    //      echo $result . "\n\n";
    // }


    public function fcmMethodResponse($data,$checkType=null){
      
        define( 'API_ACCESS_KEY',  $this->CI->config->item('FIREBASE_API_KEY_SHOPPER'));

        $time = date('Y-m-d H:i:s');
                $time = new DateTime($time);
                $minutes_to_add = 1;
                $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                $stamp = $time->format('M d,Y h:i A');
        if($checkType == ""){
            $checkType = "OPEN_ACTIVITY_1_U";
        }


        if($checkType == "register"){
            $fcmMsg = array(
                'body' => $this->CI->config->item('fcm_title'),
                'title' => $data['fcm_title'],
                'sound' => "default",
                'color' => "#203E78" ,
                "estimate"=>   100
            );
        }
        else{

            $fcmMsg = array(
                'body' => $this->CI->config->item('fcm_title'),
                'title' => $data['fcm_title'],
                'sound' => "default",
                'color' => "#203E78" ,
                "estimate"=>    100,
                'click_action'=>$checkType
            );
        }



        $fcmFields = array(
            'to' => $data['to_user'],
            'priority' => 'high',
            'notification' => $fcmMsg
        );

        if($data['data'] != ""){
            $fcmFields['data'] = $data['data']; 
        }
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        
        return $result;
    }


  /* fcm send notification to multiple user*/
  public function fcmMethod_multiple($registeration_ids,$notification,$userType=null,$data=null){
    
    define( 'API_ACCESS_KEY',  $this->CI->config->item('FIREBASE_API_KEY'));
    $fields = array(
        'registration_ids' => $registeration_ids,
        'notification'=>$notification
    );
    $headers = array(
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );  

    if($data != ""){
        $fields['data'] = $data;
    }  

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec( $ch );
    curl_close( $ch );
    
    return $result;

  }

    /*for shopper </-----*/
    public function fcmMethod_multiple_shopper($registeration_ids,$notification,$data=null){
        
        define( 'API_ACCESS_KEY',  $this->CI->config->item('FIREBASE_API_KEY_SHOPPER'));
        $fields = array(
            'registration_ids' => $registeration_ids,
            'notification'=>$notification
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );  

        if($data != ""){
            $fields['data'] = $data;
        }  

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec( $ch );
        curl_close( $ch );
        
        // return $result;
        echo "<pre>";
        print_r($result);
        echo "</pre>";
     

    }
    /*for shopper ---/> */

    /*for customer  </-----*/
    public function fcmMethod_multiple_customer($registeration_ids,$notification,$data=null){
        
        define( 'API_ACCESS_KEY',  $this->CI->config->item('FIREBASE_API_KEY_CUSTOMER'));
        $fields = array(
            'registration_ids' => $registeration_ids,
            'notification'=>$notification
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );  

        if($data != ""){
            $fields['data'] = $data;
        }  

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec( $ch );
        curl_close( $ch );
        
        return $result;

    }
    /*for customer ---/> */

    /*for dispatcher </-----*/
    public function fcmMethod_multiple_dispatcher($registeration_ids,$notification,$data=null){
        
        define( 'API_ACCESS_KEY',  $this->CI->config->item('FIREBASE_API_KEY_SHOPPER_DISPATCHER'));
        $fields = array(
            'registration_ids' => $registeration_ids,
            'notification'=>$notification
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );  

        if($data != ""){
            $fields['data'] = $data;
        }  

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec( $ch );
        curl_close( $ch );
        
        return $result;

    }
    /*for dispatcher ---/> */

    public function readFile($path) {
        $rows = array();
        $file_handle = fopen($path, 'r');
        while (!feof($file_handle) ) {
            $rows[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);

        return $rows;
    }

    
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        $resp = json_encode($response);
        $resp = trim($resp);
        echo $resp;
    }

    /*made by ranjana */

    public function fcmMethod($data,$checkType=null,$userType='customer',$typeValue=null){
        $type = '0';
        if($userType == "shopper"){
            define( 'API_ACCESS_KEY', $this->CI->config->item('FIREBASE_API_KEY_SHOPPER'));
            $type = '2';
        }
        else if($userType == "customer"){
            $type = '2';
            define( 'API_ACCESS_KEY_2', $this->CI->config->item('FIREBASE_API_KEY_CUSTOMER'));
        }
        else if($userType == "dispatcher"){
            define( 'API_ACCESS_KEY_3', $this->CI->config->item('FIREBASE_API_KEY_DISPATCHER'));
            $type = '2';
        }
        
         if($typeValue != null){
            $type = $typeValue;
        }
   
        $time = date('Y-m-d H:i:s');
        $time = new DateTime($time);
        $minutes_to_add = 1;
        $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
        $stamp = $time->format('M d,Y h:i A');
       
        
        $fcmMsg = array(
        'body' => $data['fcm_body'],
        'title' => $data['fcm_title'],
        'sound' => "default",
        'color' => "#203E78" ,
        "estimate"=> 100
        );
        
        if($checkType != "" && $checkType != null){
            $fcmMsg['click_action'] = $checkType;
        }
        
        $fcmFields = array(
        'to' => $data['to_user'],
        'priority' => 'high',
        'notification' => $fcmMsg
        );

        if($userType=='shopper'){
         $headers = array(
             'Authorization: key=' . API_ACCESS_KEY,
             'Content-Type: application/json'
             ); 
        }
        else if($userType=='customer'){
         $headers = array(
             'Authorization: key=' . API_ACCESS_KEY_2,
             'Content-Type: application/json'
            );
        }
        else if($userType=='dispatcher'){
         $headers = array(
             'Authorization: key=' . API_ACCESS_KEY_3,
             'Content-Type: application/json'
            ); 
        }
        $body_data_obj = (object) array('name' => $data['fcm_body'],'type' => $type);
        $body_obj = (object) array('data'=>$body_data_obj);
        
       
        $fcmFields['data'] = $body_obj; 

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        
        //  echo $result . "\n\n";
         return $result;
      
    }
    

 public function sendNotificationTopic($topicsArray,$checkType=null){


  
    $api_access_key = "";
    $topic = $topicsArray['topic'];
    $user_type = $topicsArray['user_type'];
    $fcm_title = $topicsArray['fcm_title'];
    $message = $topicsArray['message'];

    if($user_type == "shopper"){
        $api_access_key =  $this->CI->config->item('FIREBASE_API_KEY_SHOPPER');
    }
    else if($user_type == "customer"){
        $api_access_key =  $this->CI->config->item('FIREBASE_API_KEY_CUSTOMER');
    }
    else if($user_type == "dispatcher"){
        $api_access_key = $this->CI->config->item('FIREBASE_API_KEY_DISPATCHER');
    }
    define( 'API_ACCESS_KEY_4',$api_access_key);

    

    $fcmMsg = array(
        'body' => $message,
        'title' => $fcm_title,
        'sound' => "default",
        'color' => "#203E78" ,
        "estimate"=>    100
    );

    if($checkType != "" && $checkType != null){
        $fcmMsg['click_action'] = $checkType;
    }


    $fcmFields = array(
        'to' => '/topics/'.$topic
    );
    
    $headers = array(
        'Authorization: key=' . API_ACCESS_KEY_4,
        'Content-Type: application/json'
    ); 

    $body_data_obj = (object) array('name' => $message,'type' => '1');
    $body_obj = (object) array('to' => '/topics/'.$topic,'data'=>$body_data_obj);

    $fcmFields['data'] = $body_obj; 
    $fcmFields['notification'] = $fcmMsg; 
    
    
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
    // echo $result;
    return $result;
  
}


      /* fcm send notification to multiple user*/
  public function senNotification_multiple($registration_ids,$checkType,$notification,$user_type,$data=null){
    $api_access_key ="";
    if($user_type == "shopper"){
        $api_access_key =  $this->CI->config->item('FIREBASE_API_KEY_SHOPPER');
    }
    else if($user_type == "customer"){
        $api_access_key =  $this->CI->config->item('FIREBASE_API_KEY_CUSTOMER');
    }
    else if($user_type == "dispatcher"){
        $api_access_key = $this->CI->config->item('FIREBASE_API_KEY_DISPATCHER');
    }
    define( 'API_ACCESS_KEY_5',$api_access_key);

    $fields = array(
        'registration_ids' => $registration_ids,
        'data' =>  array ("message" => $notification,'type' => '1'),
        'notification' => array (
            "body" => $notification,
            "title" => "Boogie Notification",
          )
    );
    $headers = array(
        'Authorization: key=' . API_ACCESS_KEY_5,
        'Content-Type: application/json'
    );  

    if($data != ""){
        $fields['data'] = $data;
    }  

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec( $ch );
    curl_close( $ch );
    
    return $result;

  }

public function sendmail($to, $bodyData, $subject){
    $config = array(
        'protocol' => 'smtp',
        'smtp_host' => 'mail.stsmentor.com',
        'smtp_port' => 587,
        'smtp_user' => 'story_cap@stsmentor.com',
        'smtp_pass' => 'story_cap',
        'mailtype' => 'html',
        'charset' => 'utf-8'
    );
    $this->CI->email->initialize($config);
    $from = "noreply@sachtechsolution.com";
    $this->CI->email->from("noreply@stsmentor.com");
    $this->CI->email->to($to);
    
    $this->CI->email->subject($subject);
    $this->CI->email->message($bodyData);
    $this->CI->email->set_mailtype("html");
    $this->CI->email->set_newline("\r\n");
    $result = $this->CI->email->send();
    if($result){
        return true;
    }
    return false;
}



}

?>