<?php
class Dbresults
{
    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
    }

    public function get_data($table_name, $params, $where, $order_by = null, $limit = null, $join = null, $where_in = null)
    {
        $this->CI->db->select($params);
        $this->CI->db->from($table_name);
        if ($order_by) {
            foreach ($order_by as $key => $value) {
                $this->CI->db->order_by($key, $value);
            }
        }
        if ($limit) {
            foreach ($limit as $key => $value) {
                $this->CI->db->limit($key, $value);
            }
        }
        if ($join) {
            foreach ($join as $coll => $value) {
                $this->CI->db->join($coll, $value);
            }
        }
        if ($where_in) {
            foreach ($where_in as $coll => $value) {
                $this->CI->db->where_in($coll, $value);
            }
        }
        $this->CI->db->where($where);
        $query = $this->CI->db->get();

        if ($query) {
            $rows = $query->num_rows() > 0;
            if ($rows) {
                return $query->result();
            }
        }
        return false;
    }
    public function post_data($table_name, $data)
    {
        $this->CI->db->insert($table_name, $data);
        $proj_id = $this->CI->db->insert_id();
        if ($proj_id > 0) {
            return $proj_id;
        }
        return false;
    }
    
    public function post_batch($table_name, $data)
    {
        $proj_id = $this->CI->db->insert_batch($table_name,$data);
        
        if ($proj_id > 0) {
            return $proj_id;
        }
      
        return false;
    }

    public function update_batch($table_name, $data,$column_id)
    {
        $proj_id = $this->CI->db->update_batch($table_name,$data,$column_id);
          
        if ($proj_id > 0) {
            return $proj_id;
        }
        return false;
    }

    public function update_data($table_name, $data, $where)
    {
        $this->CI->db->where($where);
        $this->CI->db->update($table_name, $data);
        $afftectedRows = $this->CI->db->affected_rows();
        if ($afftectedRows > 0) {
            return true;
        }
        return false;
    }

    public function delete_data($table_name, $where)
    {
        $this->CI->db->where($where);
        $this->CI->db->delete($table_name);
        $afftectedRows = $this->CI->db->affected_rows();

        if ($afftectedRows > 0) {
            return true;
        }
        return false;
    }

    public function gmail_sendmail($to, $bodyData, $subject,$from){
        
        $this->CI->load->library('email');
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.stsmentor.com',
            'smtp_port' => 587,
            'smtp_user' => 'story_cap@stsmentor.com',
            'smtp_pass' => 'story_cap',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );
        $this->CI->email->initialize($config);
        if($from == ""){
            $from = "story_cap@stsmentor.com";
        }

        $this->CI->email->from("noreply@stsmentor.com");
        $this->CI->email->to($to);
        
        $this->CI->email->subject($subject);
        //$this->CI->email->header("Anamel App");
        $this->CI->email->message($bodyData);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");
        $result = $this->CI->email->send();
        if($result){
            $response['Status'] = true;
            $response['Message'] = "Email Send Successfully";
        }
        else{
            $response['Status'] = false;
            $response['Message'] = "Error in Email Send";
        }
        return $response;
    }
  
}