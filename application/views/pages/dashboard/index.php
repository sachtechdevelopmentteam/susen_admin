<div class="row mt-5">
  <div class="col-xl-12 mb-5 mb-xl-0 pl-0 pr-0">
    <div class="card shadow p-3">
      <div class="card-header rounded">
        <div class="row">
            <div class="col">
                <h3 class="mb-0">Dashboard Management</h3>
            </div>
            <div class="col" id="page_error"></div>
            <div class="col text-right">
                <!-- <button class='btn btn-info' onclick="onAdd()" id="addbtn" style="display:none" data-target = "#albumAdd" data-toggle = "modal"><i class="fa fa-plus"></i> Add New Blank Albums</button> -->
            </div>
        </div>
      </div>
      <div class="row px-3">
        <div class="col-md-7 card-header rounded mt-3" style="height:450px;overflow-y:scroll">
          <table class="table align-items-center table-flush dataTable" id="table">
            <thead class="thead-light">
              <tr>
                <th scope="col" class="text-center">S.No</th>
                <th scope="col" class="text-center">Profile Pic</th>
                <th scope="col" class="text-center">Name</th>
                <th scope="col" class="text-center">Email</th>
                <th scope="col" class="text-center">Gender</th>
              </tr>
            </thead>
            <tbody id="users_data"></tbody>
          </table>
        </div>
        <div class="col-md-5 card-header rounded mt-3">
        <div id="container2"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php 
  $this->load->view('./layouts/dashboard/footer');
?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="<?php echo base_url()?>assets/libs/js/dashboard.js"></script>