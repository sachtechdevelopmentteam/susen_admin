<div class="row mt-5">
    <div class="col-xl-12 mb-5 mb-xl-0 pl-0 pr-0">
        <div class="card shadow p-3">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="mb-0">Captured Screenshots Detail</h3>
                    </div>
                    <div class="col" id="page_error"></div>
                    <div class="col text-right">
                        <!-- <button class='btn btn-info' onclick="onAdd()" id="addbtn" style="display:none" data-target = "#albumAdd" data-toggle = "modal"><i class="fa fa-plus"></i> Add New Blank Albums</button> -->
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <!-- Projects table -->
                <table class="table align-items-center table-flush dataTable bg-white" id="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="text-center">S.No</th>
                            <th scope="col" class="text-center">ScreenShot</th>
                            <th scope="col" class="text-center">Who Captured</th>
                            <th scope="col" class="text-center">Origin UserName</th>
                            <th colspan='2' scope="col" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="users_data">
                        <tr>
                            <td class=text-center>1</td>
                            <td class=text-center><img src='assets/img/theme/vue.jpg' class="img-thumbnail" style="height:40px" /></td>
                            <td class=text-center><label onclick="userDetail('1')">Sunil Bhatia</label></td>
                            <td class=text-center><label onclick="userDetail('1')">Kapil Dhawan</label></td>
                            <td class=text-center><button onclick="onDelete('1')" class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button></td>
                        </tr>
                        <tr>
                            <td class=text-center>2</td>
                            <td class=text-center><img src='assets/img/theme/vue.jpg' class="img-thumbnail" style="height:40px" /></td>
                            <td class=text-center><label onclick="userDetail('1')">Kapil Dhawan</label></td>
                            <td class=text-center><label onclick="userDetail('1')">Sunil Bhatia</label></td>
                            <td class=text-center><button onclick="onDelete('1')" class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php 
    $this->load->view('./layouts/dashboard/footer');
?>
<script src="<?php echo base_url()?>assets/libs/js/screenshots.js"></script>