<div class="container mt-7 pb-5">
  <div class="row justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="text-center mb-5">
        <img src="<?php echo base_url() ?>assets/img/brand/logo.png"/>
        <!-- <h1 class="text-light">Logo Here</h1> -->
      </div>
      <div class="card bg-secondary shadow border-0">
        <div class="card-body px-lg-5 py-lg-5">
          <div class="text-center text-muted mb-4">
            <h3>Hi ! Admin <br>Please Put your credentials here.</h3>
          </div>
          <div class="text-center text-muted mb-1">
            <label id="login_error" style="color:red"></label>
          </div>
          <form role="form">
            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                </div>
                <input class="form-control" placeholder="Email" type="email" id="admin_email">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-lock"></i></span>
                </div>
                <input class="form-control" placeholder="Password" type="password" id="admin_password">
              </div>
            </div>
            <div class="text-center">
              <button type="button" class="btn btn-primary my-4" onclick="adminLogin()">Sign in</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div> 
<?php 
  $this->load->view('./layouts/login/footer');
?>