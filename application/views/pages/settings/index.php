<link rel="stylesheet" href="<?php echo base_url()?>assets/css/select2.css" />
<div class="row mt-5">
    <div class="col-xl-12 mb-5 mb-xl-0 pl-0 pr-0">
        <div class="card shadow p-3">
            <div class="card-header rounded">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="mb-0">Change Panel Settings</h3>
                    </div>
                    <div class="col" id="page_error"></div>
                    <div class="col text-right">
                        <!-- <button class='btn btn-success pull-right'>Save Details</button> -->
                    </div>
                </div>
            </div>
            <div class="card-header rounded mt-3">
                <h3 class="mb-4">Change Admin Password</h3>
                <div class="row">
                    <div class="col-md-4 form-group">
                        <label>Enter Old Password</label>
                        <input type='password' id="admin_old_password" class='form-control' placeholder='Enter Old Password' />
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Enter New Password</label>
                        <input type='password' id="admin_new_password" class='form-control' placeholder='Enter Password' />
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Confirm New Password</label>
                        <input type='password' id="admin_c_new_password" class='form-control' placeholder='Repeat Password' />
                    </div>
                </div>
                <div class="col-md-12 p-0 mt-3 text-right">
                    <input type='button' onclick='changePassword()' value="Change Password" class='btn btn-info' />
                </div>
            </div>
            <div class="card-header rounded mt-3">
                <h3 class="mb-4">Change Admin Name</h3>
                <div class="row">
                    <div class="col-md-4 form-group">
                        <label>Enter Name</label>
                        <input type='text' id="admin_new_name" class='form-control' placeholder='Enter Administrator Name' />
                    </div>
                </div>
                <div class="col-md-12 p-0 mt-3 text-right">
                    <input type='button' onclick="changeAdminName()" value="Change Name Now" class='btn btn-info' />
                </div>
            </div>
            <div class="card-header rounded mt-3">
                <h3 class="mb-4">Change Admin E-Mail</h3>
                <div class="row">
                    <div class="col-md-4 form-group">
                        <label>Enter Old E-Mail</label>
                        <input type='text' id="admin_old_email" value='<?php echo $_SESSION['admin_email']?>' readonly class='form-control' placeholder='Enter User Name' />
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Enter New E-Mail</label>
                        <input type='text' class='form-control' id="admin_new_email" placeholder='Enter User Name' />
                    </div>
                </div>
                <div class="col-md-12 p-0 mt-3 text-right">
                    <input type='button' onclick="changeEmail()" value="Change E-Mail Address" class='btn btn-info' />
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
    $this->load->view('./layouts/dashboard/footer');
?>
<script src="<?php echo base_url()?>assets/js/select2.js"></script>
<script src="<?php echo base_url()?>assets/libs/js/settings.js"></script>