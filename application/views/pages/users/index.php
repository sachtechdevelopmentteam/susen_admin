<div class="row mt-5">
    <div class="col-xl-12 mb-5 mb-xl-0 pl-0 pr-0">
        <div class="card shadow p-3">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="mb-0">All Available Users</h3>
                    </div>
                    <div class="col" id="page_error"></div>
                    <div class="col text-right">
                        <!-- <button class='btn btn-info' onclick="onAdd()" id="addbtn" style="display:none" data-target = "#albumAdd" data-toggle = "modal"><i class="fa fa-plus"></i> Add New Blank Albums</button> -->
                    </div>
                </div>
            </div>
            <div class="table-responsive p-2 bg-white">
                <!-- Projects table -->
                <table class="table align-items-center table-flush dataTable" id="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" class="text-center">S.No</th>
                            <th scope="col" class="text-center">Profile Pic</th>
                            <th scope="col" class="text-center">Name</th>
                            <th scope="col" class="text-center">Email</th>
                            <th scope="col" class="text-center">Gender</th>
                            <th scope="col" class="text-center">Is Online</th>
                            <th scope="col" class="text-center">Last Login</th>
                            <th scope="col" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="users_data"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php 
    $this->load->view('./layouts/dashboard/footer');
?>
<script src="<?php echo base_url()?>assets/libs/js/users.js"></script>