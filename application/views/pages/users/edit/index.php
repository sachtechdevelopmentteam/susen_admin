<link rel="stylesheet" href="<?php echo base_url()?>assets/css/select2.css" />
<input type="hidden" value="<?php echo $user_id ?>" id="user_id" />
<div class="row mt-5">
    <div class="col-xl-12 mb-5 mb-xl-0 pl-0 pr-0">
        <div class="card shadow p-3">
            <div class="card-header rounded">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="mb-0">View User Details</h3>
                    </div>
                    <div class="col" id="page_error"></div>
                    <div class="col text-right" id='statusButton'></div>
                </div>
                <p class="text-center" id='onlineStat'></p>
            </div>
            <div class="card-header rounded mt-3">
                <div class="row">
                    <div class="col-md-3">
                        <label>Profile Picture</label><br>
                        <img src='../../assets/img/theme/no_image.png' id="profile_image" class="img-thumbnail" style="max-height:200px"/>
                    </div>
                    <div class="col-md-6 text-center mt-5" id="extraImagesView"></div>
                    <div class="col-md-3">
                        <label>Verication Image</label><br>
                        <img src='../../assets/img/theme/no_image.png' id="verification_image" class="img-thumbnail" style="max-height:200px"/>
                    </div>
                </div>
            </div>
            <div class="card-header rounded mt-3">
                <h3 class="mb-4">Account Information</h3>
                <div class="row">
                    <div class="col-md-3 form-group">
                        <label>User Name</label>
                        <input disabled type='text' class='form-control' id='name' placeholder='Enter User Name' />
                    </div>
                    <div class="col-md-3">
                        <label>Account Status</label><br>
                        <select disabled class="form-control" id="accountStatus">
                            <option value="verified">Verified</option>
                            <option value="notVerified">Not Verified</option>
                            <option value="blocked">Blocked</option>
                            <option value="hidden">Hidden</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Premium Till</label><br>
                        <input type="text" id="premiumTill" readonly class="form-control" placeholder="20-Dec-2022 12:00 AM" />
                    </div>
                    <div class="col-md-3">
                        <label>Last Login</label><br>
                        <input type="text" id="lastLogin" readonly class="form-control" placeholder="Last Login Date & Time" />
                    </div>
                </div>
            </div>
            <div class="card-header rounded mt-3">
                <h3 class="mb-4">Biography</h3>
                <div class="row">
                    <div class="col-md-4">
                        <label>About Me</label><br>
                        <textarea disabled rows="2" id="aboutMe" class="form-control" placeholder="About Me"></textarea>
                    </div>
                    <div class="col-md-4">
                        <label>About Family</label><br>
                        <textarea disabled id="aboutFamily" rows="2" class="form-control" placeholder="About Family"></textarea>
                    </div>
                    <div class="col-md-4">
                        <label>Location</label><br>
                        <textarea disabled rows="2" id="location" class="form-control" placeholder="My Location"></textarea>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label>Languages</label><br>
                        <select disabled class="form-control language-list" multiple id="languages"></select>
                    </div>
                    <div class="col-md-3">
                        <label>Nationality</label><br>
                        <select disabled class="form-control" id="nationality">
                            <option value="" selected="selected">Select</option>
                            <option value="America">America</option>
                            <option value="Canada">Canada</option>
                            <option value="India">India</option>
                            <option value="Kuwait">Kuwait</option>
                            <option value="UAE">UAE</option>
                            <option value="Mexico">Mexico</option>
                            <option value="Iran">Iran</option>
                            <option value="Australia">Australia</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Origin</label><br>
                        <select disabled class="form-control" id="origin">
                            <option value="" selected="selected">Select</option>
                            <option value="America">America</option>
                            <option value="Canada">Canada</option>
                            <option value="India">India</option>
                            <option value="Kuwait">Kuwait</option>
                            <option value="UAE">UAE</option>
                            <option value="Mexico">Mexico</option>
                            <option value="Iran">Iran</option>
                            <option value="Australia">Australia</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-header rounded mt-3">
                <h3 class="mb-4">Personal Information</h3>
                <div class="row">
                    <div class="col-md-3">
                        <label>User E-Mail</label>
                        <input disabled type='text' class='form-control' id="email" disabled placeholder='Enter User Email Address' />
                    </div>
                    <div class="col-md-3">
                        <label>Gender</label>
                        <select disabled class='form-control' id="gender" >
                            <option value="" selected="selected">Select</option>
                            <option value="Male">Male</option>
                            <option value="Female" >Female</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>D.O.B</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <input disabled type="text" class="form-control" id="dob" placeholder="Enter Select Date of Birth">
                            <div class="input-group-prepend" style="width:50%">
                                <input class='form-control' style="border-radius:0" readonly id="age" placeholder="25 Years" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>Marital Status</label><br>
                        <select disabled class="form-control" id="maritalStatus">
                            <option value="" selected="selected">Select</option>
                            <option value="Single">Single</option>
                            <option value="Divorced">Divorced</option>
                            <option value="Widowed">Widowed</option>
                            <option value="Annulled">Annulled</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label>Current Living Arrangement</label><br>
                        <select disabled class="form-control" id="currentLivingArrangment">
                            <option value="" selected="selected">Select</option>
                            <option value="Living Alone">Living Alone</option>
                            <option value="Shared Housing">Shared Housing</option>
                            <option value="Boarding home">Boarding home</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Living Arrangement After Marriage</label><br>
                        <select disabled class="form-control" id="livingArrangmentAfterMarriage">
                            <option value="" selected="selected">Select</option>
                            <option value="Stay with Parents">Stay with Parents</option>
                            <option value="Willing to relocate">Willing to relocate</option>
                            <option value="Live Separately">Live Separately</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Education</label><br>
                        <select disabled class="form-control" id="education">
                            <option value="" selected="selected">Select</option>
                            <option value="High School">High School</option>
                            <option value="College">College</option>
                            <option value="In Graduation">In Graduation</option>
                            <option value="Graduated">Graduated</option>
                            <option value="Masters">Masters</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Profession</label><br>
                        <select disabled class="form-control" id="profession">
                            <option value="" selected="selected">Select</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label>Marriage Plans</label><br>
                        <select disabled class="form-control" id="marriagePlans">
                            <option value="" selected="selected">Select</option>
                            <option value="Small">Small</option>
                            <option value="Destination">Destination</option>
                            <option value="Traditional">Traditional</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Disability</label><br>
                        <input disabled type="text" class="form-control" id="disablility" placeholder="Please Mention (If Any ?) " />
                    </div>
                </div>
            </div>
            <div class="card-header rounded mt-3">
                <h3 class="mb-4">Appearance Information</h3>
                <div class="row">
                    <div class="col-md-3">
                        <label>Build</label><br>
                        <select disabled class="form-control" id="build">
                            <option value="" selected="selected">Select</option>
                            <option value="Athletic">Athletic</option>
                            <option value="Slim">Slim</option>
                            <option value="Medium">Medium</option>
                            <option value="Large">Large</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Eye Colour</label><br>
                        <select disabled class="form-control" id="eyeColour" >
                            <option value="" selected="selected">Select</option>
                            <option value="Amber">Amber</option>
                            <option value="Brown">Brown</option>
                            <option value="Blue">Blue</option>
                            <option value="Green">Green</option>
                            <option value="Hazel">Hazel</option>
                            <option value="Red">Red &amp; Violet</option>
                            <option value="Black">Black</option>
                            <option value="Gray">Gray</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Hair Colour</label><br>
                        <select disabled class="form-control" id="hairColour" >
                            <option value="" selected="selected">Select</option>
                            <option value="Brown">Brown</option>
                            <option value="Blonde">Blonde</option>
                            <option value="Black">Black</option>
                            <option value="Auburn">Auburn</option>
                            <option value="Red">Red</option>
                            <option value="Gray">Gray &amp; White</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Height</label><br>
                        <input disabled type="text" class="form-control" id="height" placeholder="Should be in Centimeters" />
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label>Color Complexion</label><br>
                        <select disabled class="form-control" id="colorComplexion">
                            <option value="" selected="selected">Select</option>
                            <option value="Light">Light</option>
                            <option value="Pale White">Pale White</option>
                            <option value="White">White</option>
                            <option value="Fair">Fair</option>
                            <option value="Medium">Medium</option>
                            <option value="Moderate Brown">Moderate Brown</option>
                            <option value="Dark Brown">Dark Brown</option>
                            <option value="Black">Black</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-header rounded mt-3">
                <h3 class="mb-4">Other Information</h3>
                <div class="row">
                    <div class="col-md-3">
                        <label>Sect</label><br>
                        <select disabled class="form-control" id="sect">
                            <option value="" selected="selected">Select</option>
                            <option value="Sunni">Sunni</option>
                            <option value="Shia">Shia</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Hijab Wear</label><br>
                        <select disabled class="form-control" id="hijab">
                            <option value="" selected="selected">Select</option>
                            <option value="Occasionally">Occasionally</option>
                            <option value="Regular">Regular</option>
                            <option value="Always">Always</option>
                            <option value="Never">Never</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Beard Style</label><br>
                        <select disabled class="form-control" id="beard">
                            <option value="" selected="selected">Select</option>
                            <option value="Trimmed">Trimmed</option>
                            <option value="Full grown">Full grown</option>
                            <option value="Clean Shaved">Clean Shaved</option>
                            <option value="Moderate">Moderate</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Pray Time</label><br>
                        <select disabled class="form-control" id="prays">
                            <option value="" selected="selected">Select</option>
                            <option value="Daily">Daily</option>
                            <option value="Moderate">Moderate</option>
                            <option value="Always">Always</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label>Revert</label><br>
                        <select disabled class="form-control" id="revert">
                            <option value="" selected="selected">Select</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Halal</label><br>
                        <select disabled class="form-control" id="halal">
                            <option value="" selected="selected">Select</option>
                            <option value="Always">Always</option>
                            <option value="Occasionally">Occasionally</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Completed Quran</label><br>
                        <select disabled class="form-control" id="completedQuran">
                            <option value="" selected="selected">Select</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
    $this->load->view('./layouts/dashboard/footer');
?>
<script src="<?php echo base_url()?>assets/js/select2.js"></script>
<script src="<?php echo base_url()?>assets/libs/js/edituser.js"></script>