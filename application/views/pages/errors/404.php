<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>404 Error - Page Not Found</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>assets/img/brand/logo.jpg">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
    </head>
    <body style="text-align:center;margin-top:6%">
        <img  class='' src="<?php echo base_url()?>assets/img/theme/error.png" />
        
        <script src="<?php echo base_url()?>assets/js/vendor/jquery-2.2.4.min.js"></script>
        <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    </body>
</html>