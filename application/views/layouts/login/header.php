<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Rnikah :: Administration</title>
  <!-- Favicon -->
  <link href="<?php echo base_url()?>assets/img/brand/logo.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="<?php echo base_url()?>assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="<?php echo base_url()?>assets/css/argon.css?v=1.0.0" rel="stylesheet">
</head>
<body class="bg-default">
  <div class="loader text-center">
    <img class="loader_img" src="<?php echo base_url()?>assets/img/theme/loader.gif" /><br>
    <label>Loading...</label>
  </div>