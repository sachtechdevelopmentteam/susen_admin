 <!-- Footer -->
 <footer class="py-5">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        
      </div>
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?php echo base_url()?>assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url()?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://www.gstatic.com/firebasejs/6.6.0/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/6.6.0/firebase-firestore.js"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url()?>assets/js/argon.js?v=1.0.0"></script>
  <script src="<?php echo base_url()?>assets/libs/js/const.js"></script>
  <script src="<?php echo base_url()?>assets/libs/js/adminLogin.js"></script>
</body>

</html>