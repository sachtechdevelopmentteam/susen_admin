<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Rnikah :: Administration</title>
  <!-- Favicon -->
  <link href="<?php echo base_url()?>assets/img/brand/logo.jpg" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="<?php echo base_url()?>assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/vendor/@fortawesome/fontawesome-free/css/fontawesome.min.css" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="<?php echo base_url()?>assets/css/argon.css?v=1.0.0" rel="stylesheet">
  <!-- Page plugins -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css">
</head>
<body>
  <div class="loader text-center">
    <img class="loader_img" src="<?php echo base_url()?>assets/img/theme/loader.gif" /><br>
    <label>Loading...</label>
  </div>
  <input type="hidden" id="admin_id" value="<?php echo $_SESSION['admin_id']?>"/>
  <input type="hidden" id="admin_name" value="<?php echo $_SESSION['admin_name']?>"/>
  <input type="hidden" id="admin_email" value="<?php echo $_SESSION['admin_email']?>"/>
 
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="javascript:void()">
        <h2>Administrator</h2><small>Rnikah</small>
      </a>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <h3>Admin</h3>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link col-md-12" href="<?php echo base_url()."dashboard"?>">
              <i class="fa fa-cube text-info"></i> Dashboard
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()."users"?>">
              <i class="fa fa-building text-dark"></i> Users
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()."subscriptions"?>">
              <i class="fa fa-credit-card text-blue"></i> Subscription Plans
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()."subscribed_user"?>">
              <i class="fa fa-users text-red"></i> Premium Users
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()."settings"?>">
              <i class="fa fa-cogs text-warning"></i> Settings
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()."complaints"?>">
              <i class="fa fa-file text-dark"></i> Complaints
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()."feedbacks"?>">
              <i class="fa fa-comment text-purple"></i> Feedback
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()."screenshots"?>">
              <i class="fa fa-file-image text-red"></i> ScreenShots
            </a>
          </li>

        </ul>
      </div>
    </div>
  </nav>
  <!-- Sidenav -->
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo base_url()?>">
        <label>Dashboard<label>
          
        </a>
       
         
        <!-- Form -->
        <!-- <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form> -->
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="<?php echo base_url()?>assets/img/theme/dummy_user.png">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold"><?php echo $_SESSION['admin_name'] ?></span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome! <br><?php echo $_SESSION['admin_email'] ?></h6>
              </div>
              <div class="dropdown-divider"></div>
              <a href="<?php echo base_url().'logout'?>" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    <div class="header bg-default pb-2 pt-2 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          
        </div>
      </div>
    </div>
     <!-- Page content -->
     <div class="container-fluid mt--7">
    