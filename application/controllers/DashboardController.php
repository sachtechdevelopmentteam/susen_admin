<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller {
	public function index()
	{
		$isLogged = $this->session->userdata('admin_id');
		if($isLogged) {
			$this->load->view('./layouts/dashboard/header');
			$this->load->view('./pages/dashboard/index');
		}
		else{
			redirect(base_url());
		}
	}
}
