<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UsersController extends CI_Controller {
	public function index()
	{
		$isLogged = $this->session->userdata('admin_id');
		if(!$isLogged) {
			redirect(base_url().'dashboard');
		}
		else{
			$this->load->view('./layouts/dashboard/header');
	     	$this->load->view('./pages/users/index');
		}
    }
    
	public function editUser($user_id)
	{
		$isLogged = $this->session->userdata('admin_id');
		if(!$isLogged) {
			redirect(base_url().'dashboard');
		}
		else{
			$this->load->view('./layouts/dashboard/header');
	     	$this->load->view('./pages/users/edit/index',array('user_id'=>$user_id));
		}
	}
}