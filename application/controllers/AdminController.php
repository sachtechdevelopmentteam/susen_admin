<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AdminController extends CI_Controller {
	
	public function index() {
		$isLogged = $this->session->userdata('admin_id');
		if($isLogged) {
			redirect(base_url().'dashboard');
		}
		else{
			$this->load->view('./layouts/login/header');
	     	$this->load->view('./pages/login/index');
		}
	}
	
    public function setSession() {
        $response = array();  
		$requiredfields = array('admin_id','admin_name','admin_email');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $admin_id = trim($this->input->post('admin_id'));
		$admin_name = trim($this->input->post('admin_name'));
		$admin_email = trim($this->input->post('admin_email'));
		$data = array(
			'admin_id'=>$admin_id,
			'admin_email'=>$admin_email,
			'admin_name'=>$admin_name
		);  
		$response[$this->config->item('status')] = true;
      	$response[$this->config->item('message')] = $this->config->item('admin_login_success');
		$this->session->set_userdata($data);
		return $this->validator->apiResponse($response);
	}
	
	public function logout() {
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('admin_email');
		$this->session->unset_userdata('admin_name');
		redirect(base_url());
	}

	public function page_not_found() {
		$this->load->view('./pages/errors/404');
	}
}