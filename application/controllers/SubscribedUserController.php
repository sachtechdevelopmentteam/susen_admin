<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SubscribedUserController extends CI_Controller {
	public function index()
	{
		$isLogged = $this->session->userdata('admin_id');
		if(!$isLogged) {
			redirect(base_url().'dashboard');
		}
		else{
			$this->load->view('./layouts/dashboard/header');
	     	$this->load->view('./pages/subscribed_user/index');
		}
    }
}