<?php

$config['status'] = 'Status';
$config['message'] = 'Message';
$config['success'] = 'Success';
$config['error'] = 'Failure';
$config['error_code'] = 'code';
$config['data'] = 'data';
$config['user_pref'] = 'UserPref';
$config['baseUrl'] = 'baseUrl';
$config['invalid_type'] = 'Invalid type found';


/**
 * Common Messages
 */
$config['state_success'] = 'States Data Found Successfully';
$config['state_failure'] = 'Unable to Find Data';

$config['city_success'] = 'Cities Data Found Successfully';
$config['city_failure'] = 'Unable to Find Data';


/**
 * College Messages
 */
$config['college_success'] = 'College Data Found Successfully';
$config['college_failure'] = 'Unable to Find Colleges Data';

$config['college_status_update_success'] = 'College Status Changed';
$config['college_status_update_failure'] = 'Unable to Change College Status';

$config['college_delete_success'] = 'College Deleted Successfully';
$config['college_delete_failure'] = 'Unable to Delete College';

$config['college_update_success'] = 'College Details Updated';
$config['college_update_failure'] = 'Unable to Update College Details';

$config['college_add_success'] = 'New College Added';
$config['college_add_failure'] = 'Unable to Add New College';


/**
 * Technology Messages
 */
$config['technology_success'] = 'Technologies Data Found Successfully';
$config['technology_failure'] = 'Unable to Find Data';

$config['technology_status_update_success'] = 'Technology Status Changed';
$config['technology_status_update_failure'] = 'Unable to Change Technology Status';

$config['technology_delete_success'] = 'Technology Deleted Successfully';
$config['technology_delete_failure'] = 'Unable to Delete Technology';

$config['technology_update_success'] = 'Technology Details Updated';
$config['technology_update_failure'] = 'Unable to Update Technology Details';

$config['technology_add_success'] = 'New Technology Added';
$config['technology_add_failure'] = 'Unable to Add New Technology';


/**
 * Trainer Messages
 */
$config['trainer_success'] = 'Trainers Data Found Successfully';
$config['trainer_failure'] = 'Unable to Find Data';

$config['trainer_status_update_success'] = 'Trainer Status Changed';
$config['trainer_status_update_failure'] = 'Unable to Change Trainer Status';

$config['trainer_delete_success'] = 'Trainer Deleted Successfully';
$config['trainer_delete_failure'] = 'Unable to Delete Trainer';

$config['trainer_update_success'] = 'Trainer Details Updated';
$config['trainer_update_failure'] = 'Unable to Update Trainer Details';

$config['trainer_add_success'] = 'New Trainer Added';
$config['trainer_add_failure'] = 'Unable to Add New Trainer';


/**
 * Marketting Messages
 */
$config['marketting_success'] = 'Marketting Team Data Found Successfully';
$config['marketting_failure'] = 'Unable to Find Data';

$config['marketting_status_update_success'] = 'Marketting Person Status Changed';
$config['marketting_status_update_failure'] = 'Unable to Change Marketting Person Status';

$config['marketting_delete_success'] = 'Marketting Person Deleted Successfully';
$config['marketting_delete_failure'] = 'Unable to Delete Marketting Person';

$config['marketting_update_success'] = 'Marketting Person Details Updated';
$config['marketting_update_failure'] = 'Unable to Update Marketting Person Details';

$config['marketting_add_success'] = 'New Marketting Person Added';
$config['marketting_add_failure'] = 'Unable to Add New Marketting Person';

$config['marketting_login_success'] = 'Login Success';
$config['marketting_login_failure'] = 'Invalid Login Credentials';

$config['email_invalid'] = 'Email Address is not Registered with Us';


/**
 * Workshop Messages
 */
$config['workshop_success'] = 'Workshops Data Found Successfully';
$config['workshop_failure'] = 'Unable to Find Workshops Data';

$config['workshop_delete_success'] = 'Workshop Deleted Successfully';
$config['workshop_delete_failure'] = 'Unable to Delete Workshop';

$config['workshop_update_success'] = 'Workshop Details Updated';
$config['workshop_update_failure'] = 'Unable to Update Workshop Details';

$config['workshop_add_success'] = 'New Workshop Added';
$config['workshop_add_failure'] = 'Unable to Add New Workshop';

?>