<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'AdminController/index';
$route['404_override'] = 'AdminController/page_not_found';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'AdminController/index';
$route['adminlogin'] = 'AdminController/adminLogin';
$route['logout'] = 'AdminController/logout';
$route['dashboard'] = 'DashboardController/index';
$route['users'] = 'UsersController/index';
$route['users/edit/(:any)'] = 'UsersController/editUser/$1';
$route['subscriptions'] = 'SubscriptionController/index/';
$route['subscribed_user'] = 'SubscribedUserController/index/';
$route['settings'] = 'SettingsController/index/';
$route['feedbacks'] = 'FeedbacksController/index/';
$route['complaints'] = 'ComplaintsController/index/';
$route['screenshots'] = 'ScreenshotsController/index/';
$route['setadminsession'] = 'AdminController/setSession/';
?>