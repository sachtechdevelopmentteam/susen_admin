$(document).ready(function(){
    getAllUsers();
});

function userDetail(user_id) {
    window.location = baseUrl + 'users/edit/' + user_id;
}

function onBlock(user_id,status) {
    let status_value = "Block";
    if(status == 'verified'){
        status_value = "Un-Block"
    }
    let message = "<div class='row'>Are you Sure, You Want to "+status_value+" this User ? </div><br><div "+
    "class='col-md-12 text-right'><button data-dismiss='modal' class='btn btn-danger'>Cancel</button>"+
    "&nbsp;<button class='btn btn-info' onclick=blockNow('"+ user_id +"','"+ status +"')>"+status_value+
    " Now</button></div>";
    showModal('Permission', message, 'red', null, '40',false);
}

function blockNow(user_id,status){
    loader(true);
    var userRef = db.collection("user").doc(user_id);
    return userRef.update({
        accountStatus: status
    })
    .then(function() {
        loader(false);
        showModal("Success","User Status Changed Successfully","green",3000,'50',false);
        location.reload();
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}

function onDelete(user_id) {
    let message = "<div class='row'>Are you Sure, You Want to Permanently Delete this User ? </div><br><div "+
    "class='col-md-12 text-right'><button data-dismiss='modal' class='btn btn-danger'>Cancel</button>"+
    "&nbsp;<button class='btn btn-info' onclick=deleteNow('"+ user_id +"')>Delete Now</button></div>";
    showModal('Permission', message, 'red', null, '40',false);
}

function deleteNow(user_id ){
    loader(true);
    db.collection("user").doc(user_id).delete().then(function() {
        loader(false);
        showModal("Success","User has been Deleted Successfully","green",3000,'50',false);
        location.reload();
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}

function onVerify(user_id) {
    loader(true);
    var userRef = db.collection("user").doc(user_id);
    userRef.get().then(function(doc) {
        if (!doc.exists) {
            showModal("Failure","Invalid User Refrence",'red',3000,'40',false);
            loader(false);
            return false;
        }
        let extraImages = "";
        for(let i=0;i<doc.data().images.length;i++){
            extraImages += "<img src='"+doc.data().images[i]+"' class='img-thumbnail' style='max-height:100px' />" 
        }
        if(extraImages === ""){
            extraImages = "<label>No Extra Images Found Yet.....</label>";
        }
        let verificationImage = doc.data().verificationImage;
        let profilePic = doc.data().profilePic;
        if(verificationImage === ""){
            verificationImage = "assets/img/theme/no_image.png";
        }
        if(profilePic === ""){
            profilePic = "assets/img/theme/no_image.png";
        }
        let modalBody = '<div class="row"><div class="form-group col-md-3"><label>Profile Picture</label>'+
        '<img src="'+ profilePic +'" class="img-thumbnail" style="max-height:200px"/></div>'+
        '<div class="col-md-6"><img src="assets/img/theme/both_side_arrow.jpg" style="width:100%" /></div>'+
        '<div class="col-md-3 form-group"><label>Verification Image</label>'+
        '<img src="'+ verificationImage +'" class="img-thumbnail" style="max-height:200px"/>'+
        '</div></div><div class="col-md-12 mt-3 text-center"><h3 class="text-left mt-3">Extra Images</h3>'+ 
        extraImages +'</div><div class="col-md-12 mb--10 mt-3 text-right"><button data-dismiss="modal" '+
        'class="btn btn-danger">Cancel</button>&nbsp;<button class="btn btn-info" '+
        'onclick=verifyProfileNow("'+ user_id +'")>Verify Now</button></div>';
        showModal('Verification Process', modalBody, 'green', null, '50',false);
        loader(false);
    }).catch(function(error) {
        showModal("Failure","Error getting Information : "+error,'red',null,'50',false);
        loader(false);
    });
}

function verifyProfileNow(user_id){
    loader(true);
    var userRef = db.collection("user").doc(user_id);
    return userRef.update({
        accountStatus: 'verified'
    })
    .then(function() {
        loader(false);
        showModal("Success","User Successfully Verified","green",3000,'50',false);
        location.reload();
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}

function getAllUsers(){
    let items = "";
    let sno = 1;
    loader(true);
    db.collection("user").get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            let onlineStatus = "Offline";
            let fa_color = "text-red";
            if(doc.data().isOnline){
                onlineStatus = "Online";
                fa_color = "text-green";
            }
            let blockbtn = "<button  onclick=onBlock('"+ doc.id +"','blocked') class='btn btn-danger btn-block btn-sm'>Block</button>"
            if(doc.data().accountStatus === "blocked"){
                blockbtn = "<button  onclick=onBlock('"+ doc.id +"','verified') class='btn btn-primary btn-block btn-sm'>Un-Block</button>"
            }
            if(doc.data().accountStatus == "notVerified"){
                blockbtn = "<button  onclick=onVerify('"+doc.id+"') class='btn btn-info btn-block btn-sm'>Verify Now</button>"
            }
            items +="<tr><td class='text-center'>"+ sno +"</td><td class=text-center>"+
                "<img src='"+ doc.data().profilePic +"' class='img-thumbnail' style='height:40px' />"+
                "</td><td class=text-center>"+doc.data().name+"</td><td class=text-center>"+
                doc.data().email +"</td>"+
                "<td class=text-center>"+ doc.data().gender +"</td><td class=text-center>"+
                "<i class='fa fa-circle "+fa_color+"'> "+onlineStatus+"</i></td><td class=text-center>"+ 
                toHumanTime(doc.data().createdOn) +"</td><td><div class='row'><div class='col-md-5 px-1'>"+
                blockbtn +"</div><div class='col-md-4 px-1'><button onclick=userDetail('"+ doc.id 
                +"') class='btn btn-warning btn-block btn-sm'><i class='fa fa-eye'></i> View</button></div>"+
                "<div class='col-md-3 px-1'><button onclick=onDelete('"+ doc.id +"') class='btn btn-danger "+
                "btn-block btn-sm'><i class='fa fa-trash'></i></button></div></div></td></tr>";
                sno++;
        });
        $("#users_data").html(items);
        $("#table").dataTable();
        loader(false);
    });
}   