$(document).ready(function(){
    getData();
});

function getData(){
    let items = "";
    let sno = 1;
    loader(true);
    db.collection("feedback").get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            let feednewdot = "<i class='fa fa-circle text-warning'></i>";
            let boldClass = "font-weight-bold";
            if(doc.data().read_status){
                feednewdot = "";
                boldClass = "";
            }
            items +="<tr><td>"+ sno +"</td><td><a href='javascript::void()' onclick=userDetail('" + 
            doc.data().user_id+"')>" + doc.data().user_name + "</a></td><td class='" + boldClass + "' "+
            "style = 'cursor:pointer' onclick=updateReadStatus('" + doc.id + "','" + doc.data().read_status + "');showModal('FeedBack','" + 
            escape(doc.data().feedback) + "','green',null,'50',true)>" + feednewdot+" "+doc.data().feedback + 
            "</td><td>" + toHumanTime(doc.data().timestamp) + "</td></tr>";
            sno++;
        });
        $("#feeds_data").html(items);
        loader(false);
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}   

function userDetail(user_id){
    window.location = baseUrl + 'users/edit/' + user_id;
}

function updateReadStatus(feed_id,read_status){
    if(read_status == "false"){
        var userRef = db.collection("feedback").doc(feed_id);
        return userRef.update({
            read_status: true
        })
        .then(function() {
            getData();
        });
    }
}