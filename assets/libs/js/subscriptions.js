$(document).ready(function(){
    getAllPackages();
});

function getAllPackages(){
    let items = "";
    let sno = 1;
    loader(true);
    db.collection("packages").get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            items +="<tr><td class='text-center'>"+ sno +"</td><td class=text-center>"+
                doc.data().package_name + "</td><td class=text-center>$" + doc.data().amount + 
                "</td><td class=text-center>" + doc.data().days +"</td><td class=text-center>" + 
                doc.data().package_id +"</td><td><div class='row'><div class='col-md-6 px-1'>"+
                "<button onclick=onEdit('" + doc.id + "') class='btn btn-warning btn-block "+
                "btn-sm'>Edit</button></div><div class='col-md-6 px-1'><button onclick=onDelete('" + doc.id + 
                "') class='btn btn-danger btn-block btn-sm'>Delete</button></div></div></td></tr>";
                sno++;
        });
        if(sno<=1){
            $("#packages_data").html("<tr><td colspan='6' class='text-center'>No Data Found</td></tr>");
        }else{
            $("#packages_data").html(items);
        }
        loader(false);
    });
}   

function onAdd(){
    let modalBody = "<div class='row'><div class='col-md-6 form-group'><label>Package Name</label><input "+
    "type='text' class='form-control' placeholder='Enter Package Name' id='package_name' /></div>"+
    "<div class='col-md-6 form-group'><label>Package Amount</label><input type='text' class='form-control'"+
    "placeholder='Enter Package Amount' id='package_amount' /></div></div><div class='row mt-2'>"+
    "<div class='col-md-6 form-group'><label>Subscription Days</label><input "+
    "type='text' class='form-control' placeholder='Enter Subscription Days' id='package_days' /></div>"+
    "<div class='col-md-6 form-group'><label>Package Id(for Android/iOS Store)</label><input type='text' "+
    "class='form-control' placeholder='Enter Package Id' id='package_id' /></div></div><div "+
    "class='col-md-12 pr-0 text-right' ><button class='btn btn-warning' data-dismiss='modal'>Cancel"+
    "</button>&nbsp;<button class=' btn btn-info' onclick=addPackage()>Submit</button></div>";
    showModal("Insert New Package",modalBody,"green",null,'40',false);
}

function addPackage(){
    var package_id = $("#package_id").val();
    var package_name = $("#package_name").val();
    var package_amount = $("#package_amount").val();
    var package_days = $("#package_days").val();
    if(package_id === "" || package_name === "" || package_amount === "" || package_days === ""){
        $("#modal_error").html("Please Fill all the Required Feilds to Proceed");
        return false;
    }
    if(isNaN(package_amount) || isNaN(package_days)){
        $("#modal_error").html("Package Name and Days Must be in Numbers");
        return false;
    }
    $("#modal_error").html(" ");
    loader(true);
    db.collection("packages").add({
        package_id: package_id,
        package_name: package_name,
        amount: package_amount,
        days: package_days
    })
    .then(function(docRef) {
        $("#modal_error").html("New Package added with Ref Id : "+docRef.id);
        setTimeout(function(){
            getAllPackages();
            $("#modal_error").html(" ");
            $("#modal").modal("hide");
        },1000)
    })
    .catch(function(error) {
        $("#modal_error").html("Error occured while adding new Package : "+error);
    });
}

function onDelete(package_id) {
    let message = "<div class='row'>Are you Sure, You Want to Permanently Delete this Package ? </div><br><div "+
    "class='col-md-12 text-right'><button data-dismiss='modal' class='btn btn-danger'>Cancel</button>"+
    "&nbsp;<button class='btn btn-info' onclick=deleteNow('"+ package_id +"')>Delete Now</button></div>";
    showModal('Permission', message, 'red', null, '40',false);
}

function deleteNow(package_id ){
    loader(true);
    db.collection("packages").doc(package_id).delete().then(function() {
        loader(false);
        showModal("Success","Package has been Deleted Successfully","green",3000,'50',false);
        setTimeout(function(){
            $("#modal").modal("hide");
            getAllPackages();
        },1000)
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}

function onEdit(package_id){
    loader(true);
    var packageRef = db.collection("packages").doc(package_id);
    packageRef.get().then(function(doc) {
        if (doc.exists) {
            let modalBody = "<div class='row'><div class='col-md-6 form-group'><label>Package Name</label><input "+
            "type='text' class='form-control' placeholder='Enter Package Name' value='"+doc.data().package_name+
            "' id='package_name' /></div><div class='col-md-6 form-group'><label>Package Amount</label>"+
            "<input type='text' class='form-control' value='"+doc.data().amount+"' "+
            "placeholder='Enter Package Amount' id='package_amount' /></div></div><div class='row mt-2'>"+
            "<div class='col-md-6 form-group'><label>Subscription Days</label><input "+
            "type='text' class='form-control' value='"+doc.data().days+"' placeholder='Enter Subscription Days'"+
            "id='package_days' /></div><div class='col-md-6 form-group'><label>Package Id(for "+
            "Android/iOS Store)</label><input type='text'  value='"+doc.data().package_id +
            "' class='form-control' placeholder='Enter Package Id' id='package_id' /></div></div><div "+
            "class='col-md-12 pr-0 text-right' ><button class='btn btn-warning' data-dismiss='modal'>Cancel"+
            "</button>&nbsp;<button class=' btn btn-info' onclick=editPackage('"+package_id+"')>Submit</button></div>";
            showModal("Update Package Detail",modalBody,"green",null,'40',false);
            loader(false);
        } else {
            showModal("Failure","Server Error !!! Please try agian after some time","red",3000,"40",false);
            loader(false);
        }
    }).catch(function(error) {
        showModal("Failure","Error getting Package : "+error,"red",3000,"40",false);
        loader(false);
    });
}

function editPackage(package_ref){
    var package_id = $("#package_id").val();
    var package_name = $("#package_name").val();
    var package_amount = $("#package_amount").val();
    var package_days = $("#package_days").val();
    if(package_id === "" || package_name === "" || package_amount === "" || package_days === ""){
        $("#modal_error").html("Please Fill all the Required Feilds to Proceed");
        return false;
    }
    if(isNaN(package_amount) || isNaN(package_days)){
        $("#modal_error").html("Package Name and Days Must be in Numbers");
        return false;
    }
    $("#modal_error").html(" ");
    loader(true);
    var packageRef = db.collection("packages").doc(package_ref);
    return packageRef.update({
        package_id: package_id,
        package_name: package_name,
        amount: package_amount,
        days: package_days
    })
    .then(function() {
        loader(false);
        showModal("Success","Package Successfully Updated","green",3000,'50',false);
        location.reload();
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}