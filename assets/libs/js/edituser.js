let allLanguages = ["English(US)","English(UK)","Arabic","Hindi","Mexican","Spanish","French","Other"];
$(document).ready(function() {
    $(".language-list").select2();
    $("#dob").datepicker();
    getUserData();
});

function getUserData(){
    let user_id = $("#user_id").val();
    if(user_id === "" || user_id === null || user_id === undefined){
        $("#page_error").html("Something Went Wrong : Please Try Again Later");
        return false;
    }
    loader(true);
    let userRef = db.collection("user").doc(user_id);
    userRef.get().then(function(doc) {
        if(!doc.exists){
            $("#page_error").html("Something Went Wrong with URL");
            loader(false);
            return false;
        }
        let onlineStat = "Status : <i class='fa fa-circle text-red'></i> Offline";
        if(doc.data().isOnline){
            onlineStat = "Status : <i class='fa fa-circle text-green'></i> Online";
        }
        $("#onlineStat").html(onlineStat);

        //for changeStatus Button///
        let statusButton = "";
        if(doc.data().accountStatus === 'blocked'){
            statusButton = "<button onclick=onBlock('" + doc.id +"','verified') class='btn btn-primary pull-right'>Un-Block User</button>";
        } else if(doc.data().accountStatus === 'verified' || doc.data().accountStatus === 'hidden'){
            statusButton = "<button onclick=onBlock('" + doc.id +"','blocked') class='btn btn-danger pull-right'>Block User</button>";
        } else if(doc.data().accountStatus === 'notVerified'){
            statusButton = "<button onclick=onVerify('" + doc.id +"') class='btn btn-danger pull-right'>Verify Now</button>";
        }
        $("#statusButton").html(statusButton);
        
        //get Extra Images
        let extraImages = "";
        for(let i=0;i<doc.data().images.length;i++){
            extraImages += "&nbsp;&nbsp;<img src='"+doc.data().images[i]+"' class='img-thumbnail' style='max-height:100px;max-width:100px' />" 
        }
        if(extraImages === ""){
            extraImages = "<label>No Extra Images Found Yet.....</label>";
        }
        $("#extraImagesView").html("<p>Extra Images</p> "+extraImages);
    
        //get profilePicture
        let profilePic = doc.data().profilePic;
        if(profilePic === ""){
            profilePic = "assets/img/theme/no_image.png";
        }
        $("#profile_image").attr("src",profilePic);

        //get verification Image
        let verificationImage = doc.data().verificationImage;
        if(verificationImage === ""){
            verificationImage = "assets/img/theme/no_image.png";
        }
        $("#verification_image").attr("src",verificationImage);
        
        $("#name").val(doc.data().name);
        $("#accountStatus").val(doc.data().accountStatus);
        
        ///premium till
        if(doc.data().premiumTill !== 0){
            $("#premiumTill").val(toHumanTime(doc.data().premiumTill));
        }
        else{
            $("#premiumTill").val("Non- Premium");
        }
        
        //last login
        if(doc.data().lastLogin !== 0){
            $("#lastLogin").val(toHumanTime(doc.data().lastLogin));
        }
        else{
            $("#lastLogin").val("No Login Date Found");
        }
    
        $("#aboutMe").val(doc.data().aboutMe);
        $("#aboutFamily").val(doc.data().aboutFamily);
        $("#location").val(doc.data().location);

        //languages
        var options = "";
        if(doc.data().languages !== null){
            for(var i=0;i<allLanguages.length;i++){
                if(doc.data().languages.includes(allLanguages[i])){
                    options += "<option value='"+allLanguages[i]+"' selected='selected'>"+allLanguages[i]+"</option>";
                }else{
                    options += "<option value='"+allLanguages[i]+"'>"+allLanguages[i]+"</option>"
                }
            }
            $("#languages").html(options);
        }
        else{
            for(var i=0;i<allLanguages.length;i++){
                options += "<option value='"+allLanguages[i]+"'>"+allLanguages[i]+"</option>"
            }
            $("#languages").val(options);
        }
                
        $("#nationality").val(doc.data().nationality);
        $("#origin").val(doc.data().origin);

        $("#email").val(doc.data().email);
        $("#gender").val(doc.data().gender);
        $("#dob").val(toHumanDate(doc.data().dob));
        setAge();
        $("#maritalStatus").val(doc.data().maritalStatus);
        $("#currentLivingArrangment").val(doc.data().currentLivingArrangment);
        $("#livingArrangmentAfterMarriage").val(doc.data().livingArrangmentAfterMarriage);
        $("#education").val(doc.data().education);
        $("#profession").val(doc.data().profession);
        $("#marriagePlans").val(doc.data().marriagePlans);
        $("#disablility").val(doc.data().disablility);

        $("#build").val(doc.data().build);
        $("#eyeColour").val(doc.data().eyeColour);
        $("#hairColour").val(doc.data().hairColour);
        $("#height").val(doc.data().height+" Centimeters");
        $("#colorComplexion").val(doc.data().colorComplexion);

        $("#sect").val(doc.data().sect);
        $("#hijab").val(doc.data().hijab);
        $("#beard").val(doc.data().beard);
        $("#prays").val(doc.data().prays);
        $("#revert").val(doc.data().revert);
        $("#halal").val(doc.data().halal);
        $("#completedQuran").val(doc.data().completedQuran);
        
        //funFacts
        
        loader(false);
    }).catch(function(error){
        $("#page_error").html("Something Went Wrong : "+error);
        loader(false);
        return false;
    });
}
//listener on date of birth field
function setAge() {
    if (isDate($('#dob').val())) {
        var age = calculateAge(parseDate($('#dob').val()), new Date());
        $("#age").val(age+' Years');   
    }      
}

function parseDate(dateStr) {
    var dateParts = dateStr.split("/");
    return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
}

//is valid date format
function calculateAge (dateOfBirth, dateToCalculate) {
    var calculateYear = dateToCalculate.getFullYear();
    var calculateMonth = dateToCalculate.getMonth();
    var calculateDay = dateToCalculate.getDate();
    var birthYear = dateOfBirth.getFullYear();
    var birthMonth = dateOfBirth.getMonth();
    var birthDay = dateOfBirth.getDate();
    var age = calculateYear - birthYear;
    var ageMonth = calculateMonth - birthMonth;
    var ageDay = calculateDay - birthDay;
    if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
        age = parseInt(age) - 1;
    }
    return age;
}

function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return true;
    //Declare Regex
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    if (dtArray == null)
        return false;
    //Checks for dd/mm/yyyy format.
    var dtDay = dtArray[1];
    var dtMonth = dtArray[3];
    var dtYear = dtArray[5];
    if (dtMonth < 1 || dtMonth > 12)
      return false;
    else if (dtDay < 1 || dtDay > 31)
      return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
      return false;
    else if (dtMonth == 2) {
      var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
      if (dtDay > 29 || (dtDay == 29 && !isleap))
        return false;
    }
    return true;
}

function onBlock(user_id,status){
    loader(true);
    var userRef = db.collection("user").doc(user_id);
    return userRef.update({
        accountStatus: status
    })
    .then(function() {
        loader(false);
        showModal("Success","User Status Changed Successfully","green",3000,'50',false);
        location.reload();
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}

function onVerify(user_id) {
    loader(true);
    var userRef = db.collection("user").doc(user_id);
    userRef.get().then(function(doc) {
        if (!doc.exists) {
            showModal("Failure","Invalid User Refrence",'red',3000,'40',false);
            loader(false);
            return false;
        }
        let extraImages = "";
        for(let i=0;i<doc.data().images.length;i++){
            extraImages += "<img src='"+doc.data().images[i]+"' class='img-thumbnail' style='max-height:100px' />" 
        }
        if(extraImages === ""){
            extraImages = "<label>No Extra Images Found Yet.....</label>";
        }
        let verificationImage = doc.data().verificationImage;
        let profilePic = doc.data().profilePic;
        if(verificationImage === ""){
            verificationImage = "assets/img/theme/no_image.png";
        }
        if(profilePic === ""){
            profilePic = "assets/img/theme/no_image.png";
        }
        let modalBody = '<div class="row"><div class="form-group col-md-3"><label>Profile Picture</label>'+
        '<img src="'+ profilePic +'" class="img-thumbnail" style="max-height:200px"/></div>'+
        '<div class="col-md-6"><img src="assets/img/theme/both_side_arrow.jpg" style="width:100%" /></div>'+
        '<div class="col-md-3 form-group"><label>Verification Image</label>'+
        '<img src="'+ verificationImage +'" class="img-thumbnail" style="max-height:200px"/>'+
        '</div></div><div class="col-md-12 mt-3 text-center"><h3 class="text-left mt-3">Extra Images</h3>'+ 
        extraImages +'</div><div class="col-md-12 mb--10 mt-3 text-right"><button data-dismiss="modal" '+
        'class="btn btn-danger">Cancel</button>&nbsp;<button class="btn btn-info" '+
        'onclick=verifyProfileNow("'+ user_id +'")>Verify Now</button></div>';
        showModal('Verification Process', modalBody, 'green', null, '50',false);
        loader(false);
    }).catch(function(error) {
        showModal("Failure","Error getting Information : "+error,'red',null,'50',false);
        loader(false);
    });
}

function verifyProfileNow(user_id){
    loader(true);
    var userRef = db.collection("user").doc(user_id);
    return userRef.update({
        accountStatus: 'verified'
    })
    .then(function() {
        loader(false);
        showModal("Success","User Successfully Verified","green",3000,'50',false);
        location.reload();
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}