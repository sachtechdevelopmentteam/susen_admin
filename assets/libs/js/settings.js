function changeEmail(){
    let session_email = $("#admin_email").val();
    let old_email = $("#admin_old_email").val();
    if(session_email !== old_email){
        showModal("Failure","Authentication Error",'red',3000,'40',false);
        return false;
    }
    let new_email = $("#admin_new_email").val();
    let admin_id = $("#admin_id").val();
    if(!(ValidateEmail(old_email) && ValidateEmail(new_email))){
        showModal("Failure","Please Enter Valid Email Address to Proceed",'red',3000,'40',false);
        return false;
    }
    loader(true);
    var ref = db.collection("administration").doc(admin_id);
    return ref.update({
        admin_email: new_email
    })
    .then(function() {
        loader(false);
        showModal("Success","Please Login with New E-Mail Address to Reflect Changes","green",3000,'50',false);
        setTimeout(function(){
            window.location= baseUrl+"logout";
        },3000); 
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}
 
function changeAdminName(){
    let session_name = $("#admin_name").val();
    let new_name = $("#admin_new_name").val();
    if(session_name === new_name){
        showModal("Failure","Name Must be diffrent from Previous one.",'red',3000,'40',false);
        return false;
    }
    let admin_id = $("#admin_id").val();
    loader(true);
    var ref = db.collection("administration").doc(admin_id);
    return ref.update({
        admin_name: new_name
    })
    .then(function() {
        loader(false);
        showModal("Success","Please Login with New E-Mail Address to Reflect Changes","green",3000,'50',false);
        setTimeout(function(){
            window.location= baseUrl+"logout";
        },3000); 
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}

function changePassword(){
    let old_password = $("#admin_old_password").val();
    let new_password = $("#admin_new_password").val();
    let c_new_password = $("#admin_c_new_password").val();
    let admin_id = $("#admin_id").val();
    if(old_password === "" || new_password === "" || c_new_password === ""){
        $("#page_error").html("Please Fill all the Required Fields to Proceed");
        return false;
    }
    if(new_password !== c_new_password){
        $("#page_error").html("Password and Confirm Password Should Be Same");
        return false;
    }
    loader(true);
    let ref = db.collection("administration").doc(admin_id);
    ref.get().then(function(doc) {
        if(!doc.exists){
            $("#page_error").html("Something Went Wrong!!! Please Try After Some Time...");
            loader(false);
            return false;
        }
        if(old_password !== doc.data().admin_password){
            $("#page_error").html("Incorrect Old Password....");
            loader(false);
            return false;
        }
        return ref.update({
            admin_password : new_password
        })
        .then(function() {
            loader(false);
            showModal("Success","Please Login with New Password to Access Panel","green",3000,'50',false);
            setTimeout(function(){
                window.location= baseUrl+"logout";
            },3000); 
        })
        .catch(function(error) {
            loader(false);
            showModal("Failure","Error Occured "+error,"red",null,'50',false);
        });
    })
    .catch(function(error) {
        loader(false);
        showModal("Failure","Error Occured "+error,"red",null,'50',false);
    });
}