function adminLogin(){
    var admin_email = $("#admin_email").val();
    var admin_password = $("#admin_password").val();
    if(admin_email == "" || admin_password === ""){
        $("#login_error").html("Please Enter Credentials to Login...");
        return false;
    }
    $("#login_error").html(" ");
    loader(true);
    let admin_id = "";
    let admin_name = "";
    let login = false;
    db.collection("administration").where("admin_email", "==", admin_email).where("admin_password", "==", admin_password)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            login=true;
            admin_name = doc.data().admin_name;
            admin_id = doc.id;
        });
        if(!login){
            $("#login_error").html("Invalid Login Credentials");
            loader(false);
            return false;
        }
        let url = baseUrl + "setadminsession";
        let formData = new FormData();
        formData.append('admin_id', admin_id);
        formData.append('admin_name', admin_name);
        formData.append('admin_email', admin_email);
        let xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        xhr.send(formData);
        xhr.onload = function() {
            let obj = JSON.parse(xhr.responseText);
            if (xhr.status === 200) {
                window.location=baseUrl;
            } else {
                $('#login_error').html(obj.Message);
                loader(false);
            }
        }
    })
    .catch(function(error) {
        loader(false);
        $("#login_error").html("Error getting documents: ", error);
    });
}   