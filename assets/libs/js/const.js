﻿const baseUrl = "http://localhost:8888/Rnikah/";
const API_KEY = "123456789";

function toHumanTime(timestamp) {
    if (timestamp !== "") {
        date = new Date(timestamp * 1000);
        let year = date.getFullYear();
        let month = date.getMonth();
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        let dated = date.getDate();
        let hours = date.getHours();
        let mins = date.getMinutes();
        let secs = date.getSeconds();
        return dated + "-" + months[month] + "-" + year + " " + hours + ":" + mins + ":" + secs
    }
    return "No Date";
}

function toHumanDate2(datestamp) {
    if (datestamp !== "") {
        date = new Date(datestamp * 1000);
        let year = date.getFullYear();
        let month = date.getMonth();
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        let dated = date.getDate();
        return dated + "-" + months[month] + "-" + year
    }
    return "No Date";
}

function readURL(input, output_id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#' + output_id).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function loader(option) {
    if (option) {
        $(".loader").show();
    } else {
        $(".loader").hide();
    }
}

function showModal(status, message, statusColor, timer, modalWidth,isEscape) {
    $("#modal-title-default").css("color", statusColor);
    $("#modal-title-default").html(status);
    if(isEscape){
        $(".modal-body").html(unescape(message));
    } else {
        $(".modal-body").html(message);
    }
    
    $("#modal_size").css("width", modalWidth + "%");
    $("#modal").modal("show");
    if (timer !== null) {
        setTimeout(function() {
            $("#modal").modal("hide");
        }, timer);
    }
}

function getStates() {
    let states = [];
    return new Promise((resolve, reject) => {
        let url = baseUrl + "api/v1/states";
        let xhr = new XMLHttpRequest();
        let formData = new FormData();
        formData.append('API_KEY', API_KEY);
        xhr.open('POST', url);
        xhr.send(formData);
        xhr.onload = function() {
            let obj = JSON.parse(xhr.responseText);
            if (xhr.status === 200) {
                states = obj.data;
                resolve(states);
            }
            reject(states);
        };
    });
}

function getCitiesByStateId(state_id) {
    let cities = [];
    return new Promise((resolve, reject) => {
        let url = baseUrl + "api/v1/citiesbystateid";
        let xhr = new XMLHttpRequest();
        let formData = new FormData();
        formData.append('API_KEY', API_KEY);
        formData.append('state_id', state_id);
        xhr.open('POST', url);
        xhr.send(formData);
        xhr.onload = function() {
            let obj = JSON.parse(xhr.responseText);
            if (xhr.status === 200) {
                cities = obj.data;
                resolve(cities);
            }
            reject(cities);
        };
    });
}

function getAllCities() {
    let cities = [];
    return new Promise((resolve, reject) => {
        let url = baseUrl + "api/v1/cities";
        let xhr = new XMLHttpRequest();
        let formData = new FormData();
        formData.append('API_KEY', API_KEY);
        xhr.open('POST', url);
        xhr.send(formData);
        xhr.onload = function() {
            let obj = JSON.parse(xhr.responseText);
            if (xhr.status === 200) {
                cities = obj.data;
                resolve(cities);
            }
            reject(cities);
        };
    });
}
function toHumanDate(timestamp) {
    if (timestamp !== "") {
        date = new Date(timestamp * 1000);
        let year = date.getFullYear();
        let month = date.getMonth()+1;
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        let dated = date.getDate();
        return dated + "/" + month + "/" + year
    }
    return "Invalid Format";
}
var config = {
    apiKey: "AIzaSyBZdUgbb3JBFrX5l_aNqrIVza6tNnu97Dw",
    authDomain: "susen-dating-app.firebaseapp.com",
    databaseURL: "https://susen-dating-app.firebaseio.com",
    projectId: "susen-dating-app",
    storageBucket: "susen-dating-app.appspot.com",
    messagingSenderId: "925888646660",
    appId: "1:925888646660:web:4e9cc5c3862316f7d7ef51"
};
// Initialize Firebase
firebase.initializeApp(config);
var db = firebase.firestore();

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return true;
    }
    return false;
}