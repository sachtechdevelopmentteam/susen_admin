$(document).ready(function(){
    getData();
});

function getData(){
    let items = "";
    let sno = 1;
    loader(true);
    db.collection("complaints").get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            items +="<tr><td>"+ sno +"</td><td><a href='javascript::void()' onclick=userDetail('" + 
            doc.data().report_by_user_id+"')>" + doc.data().report_by_user_name + "</a></td>"+
            "<td><a href='javascript::void()' onclick=userDetail('" + doc.data().report_to_user_id + 
            "')>" + doc.data().report_to_user_name + "</a></td><td>" + doc.data().report_reason + 
            "</td><td><i class='fa fa-trash btn btn-danger btn-sm' onclick=onDelete('" + doc.id + 
            "')></i></td></tr>";
            sno++;
        });
        $("#complaints_data").html(items);
        loader(false);
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}   

function userDetail(user_id){
    window.location = baseUrl + 'users/edit/' + user_id;
}

function onDelete(complaint_id) {
    let message = "<div class='row'>Are you Sure, You Want to Permanently Delete this Complaint ? </div><br><div "+
    "class='col-md-12 text-right'><button data-dismiss='modal' class='btn btn-danger'>Cancel</button>"+
    "&nbsp;<button class='btn btn-info' onclick=deleteNow('"+ complaint_id +"')>Delete Now</button></div>";
    showModal('Permission', message, 'red', null, '40',false);
}

function deleteNow(complaint_id ){
    loader(true);
    db.collection("complaints").doc(complaint_id).delete().then(function() {
        getData();
        showModal("Success","Complaint has been Deleted Successfully","green",2000,'50',false);
    })
    .catch(function(error) {
        loader(false);
        $("#modal_error").html("Error Occured "+error);
        setTimeout(() => {
            $("#modal").modal('hide');
        }, 2000);
    });
}