$(document).ready(function(){
    getAllUsers();
});

function userDetail(user_id) {
    window.location = baseUrl + 'users/edit/' + user_id;
}

function getAllUsers(){
    let items = "";
    let sno = 1;
    loader(true);
    db.collection("user").where("premiumTill", ">", 0)
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            let subs_status = "";
            let onlineStatus = "Offline";
            let fa_color = "text-red";
            if(doc.data().isOnline){
                onlineStatus = "Online";
                fa_color = "text-green";
            }
            var current_timeStamp = Math.round((new Date().getTime()/1000));
            if(doc.data().premiumTill > current_timeStamp){
                subs_status = "Premium";
            }
            else if(doc.data().premiumTill > 0 && doc.data().premiumTill < current_timeStamp){
                subs_status = "Subscription Expired";
            }
            items +="<tr><td class='text-center'>"+ sno +"</td><td class=text-center>"+
            "<img src='"+ doc.data().profilePic +"' class='img-thumbnail' style='height:40px' />"+
            "</td><td class=text-center><a href='javascript::void()' onclick=userDetail('"+doc.id+"') "+
            ">"+doc.data().name+"</a></td><td class=text-center>"+
            doc.data().email +"</td><td class=text-center>"+ doc.data().gender +"</td><td class=text-center>"+
            "<i class='fa fa-circle "+fa_color+"'> "+onlineStatus+"</i></td><td class=text-center>"+ 
            toHumanTime(doc.data().createdOn) +"</td><td class='text-center'>"+subs_status+
            "</td><td class='text-center'>"+toHumanTime(doc.data().premiumTill)+"</td>"+
            "<td class='text-center'>"+doc.data().accountStatus+"</td></tr>";
            sno++;
        });
        $("#users_data").html(items);
        $("#table").dataTable();
        loader(false);
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}   