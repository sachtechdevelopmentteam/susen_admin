let allusers = [];
let verified = [];
let notVerified = [];
let blocked = [];
let hidden = [];
let dates = [];
$(document).ready(function(){
    getData();
})

function getData(){
    let items = "";
    let sno = 1;
    loader(true);
    db.collection("user").get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            items +="<tr><td class='text-center'>"+ sno +"</td><td class=text-center>"+
            "<img src='"+ doc.data().profilePic +"' class='img-thumbnail' style='height:40px' />"+
            "</td><td class=text-center>"+doc.data().name+"</td><td class=text-center>"+
            doc.data().email +"</td><td class=text-center>"+ doc.data().gender + 
            "</td></tr>";
            sno++;
            allusers.push(doc.id);
            dates.push(parseInt(doc.data().createdOn));
            if(doc.data().accountStatus === 'verified'){
                verified.push(doc.id);
            }
            else if(doc.data().accountStatus === 'notVerified'){
                notVerified.push(doc.id);
            }
            else if(doc.data().accountStatus === 'blocked'){
                blocked.push(doc.id);
            }
            else if(doc.data().accountStatus === 'hidden'){
                hidden.push(doc.id);
            }
        });
        $("#users_data").html(items);
        $("#table").dataTable();
        loadCharts();
    });
}  

function loadCharts(){
    Highcharts.chart('container2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: "Pie Chart Based on User's Account Status"
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'Users',
            colorByPoint: true,
            data: [{
                name: 'Verified<br>Users<br>',
                y: ((allusers.length)*(verified.length)),
                sliced: true,
                selected: true
            }, {
                name: 'Blocked<br>Users<br>',
                y: ((allusers.length)*(blocked.length))
            }, {
                name: 'Hidden<br>Users<br>',
                y: ((allusers.length)*(hidden.length))
            }, {
                name: 'Not<br>Verified<br>',
                y: ((allusers.length)*(notVerified.length))
            }]
        }]
    });
    loader(false);
}
function userDetail(user_id) {
    window.location = baseUrl + 'users/edit/' + user_id;
}